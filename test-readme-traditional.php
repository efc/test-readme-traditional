<?php
/*
Plugin Name:            Test Readme Traditional
Description:            Does nothing at all.
Plugin URI:             https://bitbucket.org/efc/test-readme-traditional
Bitbucket Plugin URI:   https://bitbucket.org/efc/test-readme-traditional
Bitbucket Branch:       master
Version:                1.1
Author:                 Eric Celeste
Author URI:             http://eric.clst.org/
License:                The MIT License 
License URI:            http://opensource.org/licenses/MIT
*/

# do nothing at all