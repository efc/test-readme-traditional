# Test Readme Traditional

- Contributors: efc
- Tags: debugging
- Requires at least: 3.5
- Tested up to: 4.1.2
- Stable tag: master
- License: The MIT License 
- License URI: http://opensource.org/licenses/MIT

This plugin does nothing.

## Description

This plugin does nothing at all. It is here just to show this readme.

### Features

No features.

### Usage

Nothing to do.

## Installation

The easiest way to install and maintain this plugin is with [GitHub Updater](https://github.com/afragen/github-updater). The great thing about using GitHub Updater is that the plugin will then be able to be updated through the regular WordPress update process.

Of course, if you wish to install and update the plugin manually, you are welcome to do so.

### Installing with GitHub Updater
1. First make sure you have [GitHub Updater](https://github.com/afragen/github-updater) itself installed.
2. In the WordPress Dashboard go to *Settings > GitHub Updater > Install Plugin*.
3. Enter `efc/test-readme-traditional` as the Plugin URI and make sure the Remote Repository Host is `Bitbucket`. Then click the "Install Plugin".
4. Activate the plugin after it has installed.

When the plugin is updated, GitHub Updater will make sure that WordPress knows this and presents the usual offers to update the plugin for you.

## Notes

This is just for debugging purposes.
